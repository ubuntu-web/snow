# snow: v1.3

An SSB implementation for Firefox.

**We are accepting merge requests.**

## Dependencies
* `python3`
* `firefox`
* `zenity`

## License
***Copyright (c) 2020, Rudra Saraswat***

***Licensed under the BSD 3-Clause License.***
